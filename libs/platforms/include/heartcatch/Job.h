#pragma once
#ifndef _HEARTCATCH_JOB_H_
#define _HEARTCATCH_JOB_H_

namespace Heartcatch
{

    class JobHandle;

    class Job abstract
    {
    public:
        Job();
        virtual ~Job();
        void OnQueued(JobHandle *handle);
        virtual bool OnRun() = 0;
        void OnFinished();
    protected:
        
    private:
        JobHandle   *handle;
    };

}

#endif // _HEARTCATCH_JOB_H_
