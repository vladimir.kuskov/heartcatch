#pragma once
#ifndef _HEARTCATCH_JOBHANDLE_H_
#define _HEARTCATCH_JOBHANDLE_H_

#include <cstdint>
#include <atomic>

namespace Heartcatch
{

    class JobHandle final
    {
    public:
        JobHandle();
        ~JobHandle();
        bool IsFinished()const;
        void Wait();
        void AddJob();
        void ReleaseJob();
    private:
        std::atomic<uint32_t>   activeJobCount;
    private:
        JobHandle(const JobHandle&);
        JobHandle &operator=(const JobHandle&);
    };
}
#endif // _HEARTCATCH_JOBHANDLE_H_
