#pragma once
#ifndef _HEARTCATCH_JOB_MANAGER_H_
#define _HEARTCATCH_JOB_MANAGER_H_

#include <memory>

namespace Heartcatch
{

    class Job;
    class JobHandle;
    
    namespace Impl
    {
        class JobManagerImpl;
    }

    class JobManager final
    {
    public:
        JobManager();
        ~JobManager();
        bool Init();
        void Shutdown();
        void Run(JobHandle *handle, Job *job);
        void Run(JobHandle *handle, Job **jobs, size_t jobCount);
    private:
        std::unique_ptr<Impl::JobManagerImpl>   impl;
    private:
        JobManager(const JobManager&);
        JobManager &operator=(const JobManager&);
    };

    extern JobManager *jobManager;

}

#endif // _HEARTCATCH_JOB_MANAGER_H_
