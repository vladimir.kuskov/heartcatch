#include "heartcatch/JobManager.h"
#include "JobManagerImpl.h"

using namespace Heartcatch;
using namespace Heartcatch::Impl;

JobManager::JobManager()
    : impl(std::make_unique<JobManagerImpl>())
{}

JobManager::~JobManager()
{}

bool JobManager::Init()
{
    return impl->Init();
}

void JobManager::Shutdown()
{
    impl->Shutdown();
}

void JobManager::Run(JobHandle *handle, Job **jobs, size_t jobCount)
{
    impl->Run(handle, jobs, jobCount);
}

void JobManager::Run(JobHandle *handle, Job *job)
{
    Job* jobs[] = { job };
    Run(handle, jobs, 1);
}
